package io.mtksdk.viewcontroller.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import io.mtksdk.viewcontroller.fragment.BaseFragment;
import io.mtksdk.viewcontroller.fragment.SdkCloseFragment;
import io.mtksdk.viewcontroller.fragment.SdkLikertFragment;
import io.mtksdk.viewcontroller.fragment.SdkMultipleChoiceFragment;
import io.mtksdk.viewcontroller.fragment.SdkOpenEndShortFragment;
import io.mtksdk.viewcontroller.fragment.SdkOpenEndLongFragment;
import io.mtksdk.viewcontroller.fragment.SdkScaleFragment;
import io.mtksdk.viewcontroller.fragment.SdkSmileyFragment;
import io.mtksdk.viewcontroller.fragment.SdkStartFragment;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by kgy 2019. 9. 24.
 */

public class ViewControllerAdapter extends FragmentStatePagerAdapter {
    private static int NUM_ITEMS;
    protected  ArrayList<org.json.JSONObject> questions;
    private ArrayList<java.lang.Object> parseQuestions;
    private HashMap<String, ArrayList<Object>> answerMap;
    private HashMap<Integer, BaseFragment> fragments;


    public ViewControllerAdapter(FragmentManager fm, HashMap<String, ArrayList<Object>> answerMap, ArrayList<JSONObject> questions) {
        super(fm);
        this.answerMap = answerMap;
        this.questions = questions;
        this.fragments = new HashMap<>();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        NUM_ITEMS = questions.size();
        return NUM_ITEMS;
    }

    @Override
    public Fragment getItem(int position) {
        JSONObject question = questions.get(position);

        BaseFragment item = null;
        if (fragments.containsKey(position)) {
            item = fragments.get(position);
        } else {
            if (question.optString("type").equals("intro")) {
                item = SdkStartFragment.getInstance(question, answerMap);
            } else if (question.optString("type").equals("multipleChoice")) {
                item = SdkMultipleChoiceFragment.getInstance(question,answerMap);
            } else if (question.optString("type").equals("openEnd") && question.has("isShortForm")) {
                item = SdkOpenEndShortFragment.getInstance(question,answerMap);
            } else if (question.optString("type").equals("openEnd") && !question.has("isShortForm")) {
                item = SdkOpenEndLongFragment.getInstance(question,answerMap);
            } else if (question.optString("type").equals("range")) {
                item = SdkScaleFragment.getInstance(question,answerMap);
            } else if (question.optString("type").equals("likert")) {
                item = SdkLikertFragment.getInstance(question, answerMap);
            } else if (question.optString("type").equals("smiley")) {
                item = SdkSmileyFragment.getInstance(question, answerMap);
            } else if (question.optString("type").equals("outro")){
                item = SdkCloseFragment.getInstance(question, answerMap);
            }
            fragments.put(position, item);
        }
        return item;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Page" + position;
    }

}
