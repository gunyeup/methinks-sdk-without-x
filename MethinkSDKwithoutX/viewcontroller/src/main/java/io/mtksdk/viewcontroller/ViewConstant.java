package io.mtksdk.viewcontroller;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by kgy 2019. 9. 24.
 */
public class ViewConstant {
    public static String campId;
    public static ArrayList<JSONObject> questions = new ArrayList<>();
    public static ArrayList<Object> openEndAnswers = new ArrayList<>();
    public static JSONObject answer = new JSONObject();
    protected static NetworkManager nm = new NetworkManager();
    public static String apiKey = "";
    public static String deviceId = "";
    public static String surveyId = "";
    public static Boolean hasSurveyed = false;
    public static Boolean isSkippable = true;

    public static void surveyCompletion(String aKey, String devId, String surveyId, JSONObject answer) {
        try {
            nm.suveryCompletion(aKey, devId, surveyId, answer, new NetworkManager.CallbackInterface() {
                @Override
                public void onDownloadSuccess(boolean success, JSONObject response) {
                    Log.i("surveyCompletion", response.toString());
                    ViewConstant.answer = new JSONObject();
                }

                @Override
                public void onDownloadFail(boolean fail, Throwable e) {
                    Log.i("surveyCompletion", e.toString());
                    ViewConstant.answer = new JSONObject();
                }
            });
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static String getSurvIdView(String key, Context activity) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        SharedPreferences.Editor editor = prefs.edit();
        if (prefs.contains(key)) {
            String survId = prefs.getString(key, null);
            return survId;
        } else {
            return null;
        }
    }

    public static String getAuthKeyView(String key, Context activity) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        if (prefs.contains(key)) {
            return prefs.getString(key, null);
        } else {
            return null;
        }
    }
}
