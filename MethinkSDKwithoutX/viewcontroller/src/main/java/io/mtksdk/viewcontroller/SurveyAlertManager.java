package io.mtksdk.viewcontroller;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class SurveyAlertManager extends AppCompatActivity {

    protected static Context thirdUser;
    protected static NetworkManager nm = new NetworkManager();
    protected static ViewControllerManager vcm = new ViewControllerManager();

    public static void showDialog(final Context act, final String aKey, final String surveyId, final String devId, Boolean isSkippable) {
        thirdUser = act;

        ViewConstant.apiKey = aKey;
        ViewConstant.deviceId = devId;
        ViewConstant.surveyId = surveyId;
        ViewConstant.isSkippable = isSkippable;

        if (ViewConstant.questions != null && !ViewConstant.questions.isEmpty()) {
            ViewConstant.questions.clear();
        }
        try {
            nm.getSurveyQuestion(ViewConstant.apiKey, ViewConstant.surveyId, new NetworkManager.CallbackInterface() {
                @Override
                public void onDownloadSuccess(boolean success, JSONObject response) {
                    Log.i("surveyQ", response.toString());
                    if (response.optJSONObject("result").optJSONArray("questions") != null) {
                        for (int i = 0; i < response.optJSONObject("result").optJSONArray("questions").length(); i++) {
                            Log.i("surveyQuestion", response.optJSONObject("result").optJSONArray("questions").optJSONObject(i).toString());
                            JSONObject question = response.optJSONObject("result").optJSONArray("questions").optJSONObject(i);
                            ViewConstant.questions.add(question);
                        }
                        Intent myIntent = new Intent(thirdUser, ViewControllerManager.class);
                        thirdUser.startActivity(myIntent);
                    }
                }

                @Override
                public void onDownloadFail(boolean fail, Throwable e) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static JSONObject createJSObj(String key, String value) {
        JSONObject temp = new JSONObject();
        try {
            temp.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return temp;
    }
}
