package io.mtksdk.viewcontroller;

import android.app.Activity;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class NetworkManager extends Activity {

    final String METHINKS_URL_CAMPAIGNITEM = "https://dev.methinks.io/parse/functions/getMyCampaignItems";
    final String METHINKS_URL_CAMPAIGNSUBITEM = "https://dev.methinks.io/parse/functions/getCheckInQuestionsWithCheckinList";
    final String METHINKS_SDK_URL_SURVEYQUESTION = "https://sdk-dev.methinks.io/parse/functions/getSurveyQuestions";
    final String METHINKS_SDK_URL_COMPLETE = "https://sdk-dev.methinks.io/parse/functions/surveyCompletion";



    public interface CallbackInterface {
        public void onDownloadSuccess(boolean success, JSONObject response);
        public void onDownloadFail(boolean fail, Throwable e);
    }

    public void getSurveyQuestion(String aKey, String surveyId, final NetworkManager.CallbackInterface callback) throws UnsupportedEncodingException {
        try {
            AsyncHttpClient clientQuestion = new AsyncHttpClient();

            clientQuestion.addHeader("X-Parse-Application-Id", "mySDKAppId");
            clientQuestion.addHeader("X-Parse-REST-API-Key", "myRESTAPIKey");

            JSONObject params = new JSONObject();
            params.put("aKey", aKey);
            params.put("surveyId", surveyId);

            StringEntity entity = new StringEntity(params.toString());


            clientQuestion.post(this, METHINKS_SDK_URL_SURVEYQUESTION, entity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    callback.onDownloadSuccess(true, response);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject response) {
                    callback.onDownloadFail(true, e);

                    Log.e("getSurveyQuestion", "Fail: " + e.toString());
                    Log.d("getSurveyQuestion", "StatusCode :" + statusCode);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void suveryCompletion(String aKey, String devId, String surveyId, JSONObject answer, final CallbackInterface callback) throws UnsupportedEncodingException {
        try {
            AsyncHttpClient clientSurvComp = new AsyncHttpClient();

            clientSurvComp.addHeader("X-Parse-Application-Id", "mySDKAppId");
            clientSurvComp.addHeader("X-Parse-REST-API-Key", "myRESTAPIKey");

            JSONObject params = new JSONObject();
            params.put("aKey", aKey);
            params.put("devId", devId);
            params.put("os", "android");
            params.put("surveyId", surveyId);
            if (answer != null && answer.length() != 0) {
                params.put("answers", answer);
            }

            StringEntity entity = new StringEntity(params.toString());

            clientSurvComp.post(this, METHINKS_SDK_URL_COMPLETE, entity, "application/json", new JsonHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    callback.onDownloadSuccess(true, response);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject response) {
                    callback.onDownloadFail(true, e);

                    Log.e("suveryCompletion", "Fail: " + e.toString());
                    Log.d("suveryCompletion", "StatusCode :" + statusCode);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
