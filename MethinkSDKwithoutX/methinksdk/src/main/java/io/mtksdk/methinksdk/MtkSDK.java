package io.mtksdk.methinksdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import io.mtksdk.activitylogmanager.ActivityLogConstant;
import io.mtksdk.activitylogmanager.SdkLifecycle;
import io.mtksdk.fingerprintmanager.FingerPrintManager;
import io.mtksdk.networkmanager.NetworkConstant;
import io.mtksdk.networkmanager.NetworkConnect;
import io.mtksdk.viewcontroller.SurveyAlertManager;
import io.mtksdk.viewcontroller.ViewConstant;

public class MtkSDK {

    protected static String uuid;
    protected static String apiKey;
    protected static String locale;
    protected static String timeZoneAbbr;
    protected static NetworkConnect nm;
    protected static Application currApp;
    protected static Context thirdUser;
    protected static ArrayList<JSONArray> prevSData;
    protected static String prevSid;
    protected static Boolean prevSIsNew;

    public static void start(Activity activity) {
        init(activity);
    }

    protected static void init(final Activity activity) {
        try {
            ActivityLogConstant.isInit = true;
            nm = new NetworkConnect();
            currApp = activity.getApplication();
            thirdUser = activity;
            locale = activity.getResources().getConfiguration().locale.toString();
            timeZoneAbbr = NetworkConstant.getTimeZoneAbbr();
            prevSData = ActivityLogConstant.getArrayList("saveList", thirdUser);
            prevSid = ActivityLogConstant.getSid("saveSid", thirdUser);
            prevSIsNew = ActivityLogConstant.getSIsNew("saveSIsNew", thirdUser);
            NetworkConstant.sId = NetworkConstant.getAlphaNumericString(12);
            ApplicationInfo ai = activity.getPackageManager().getApplicationInfo(activity.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            apiKey = bundle.getString("MtkSDK_API_KEY");
            ActivityLogConstant.actAuthKey = apiKey;

            uuid = ActivityLogConstant.getUUID("UUID", thirdUser);
            if (uuid == null || uuid == "unknown") {
                uuid = FingerPrintManager.getUuid();
                ActivityLogConstant.saveUUID("UUID", uuid, thirdUser);
            }

            if (ActivityLogConstant.getAuthKey("aKey", thirdUser) == null || !ActivityLogConstant.getAuthKey("aKey", thirdUser).equals(apiKey)) {
                ActivityLogConstant.saveAuthKey("aKey", apiKey, thirdUser);
            }

            JSONObject isKilled = ActivityLogConstant.getIsKilled("isKilled", thirdUser);
            if (isKilled == null || !isKilled.optString("aKey").equals(apiKey) || !isKilled.optBoolean("isKilled")) {
                if (ActivityLogConstant.getViewDicVer("viewDicVer", thirdUser) != null && ActivityLogConstant.getViewDicVer("viewDicVer", thirdUser) != -1) {
                    NetworkConstant.viewDicVer = ActivityLogConstant.getViewDicVer("viewDicVer", thirdUser);
                }
                if (ActivityLogConstant.getScreenVer("screenVer", thirdUser) != null && ActivityLogConstant.getScreenVer("screenVer", thirdUser) != -1) {
                    NetworkConstant.screenVer = ActivityLogConstant.getScreenVer("screenVer", thirdUser);
                }
                nm.setSDKUser(apiKey, uuid, "android", locale, timeZoneAbbr, prevSData, prevSid, prevSIsNew, new NetworkConnect.CallbackInterface() {
                    @Override
                    public void onDownloadSuccess(boolean success, JSONObject response) {
                        Log.i("setSDKUser", response.toString());
                        if (response.optJSONObject("result").has("isKilled")) {
                            try {
                                JSONObject isKilledDic = new JSONObject();
                                isKilledDic.put("aKey", apiKey);
                                isKilledDic.put("isKilled", response.optJSONObject("result").optBoolean("isKilled"));
                                ActivityLogConstant.saveIsKilled("isKilled", isKilledDic, thirdUser);
                                NetworkConstant.isActive = false;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        if (NetworkConstant.isActive != null && !NetworkConstant.isActive && response.optJSONObject("result").optBoolean("isActive")) {
                            NetworkConstant.isActive = true;
                        }
                        if (NetworkConstant.isActive != null && !NetworkConstant.isActive) {
                            ActivityLogConstant.saveIsActive("isActive", NetworkConstant.isActive, thirdUser);
                        } else {
                            NetworkConstant.isActive = response.optJSONObject("result").optBoolean("isActive");
                            ActivityLogConstant.saveIsActive("isActive", NetworkConstant.isActive, thirdUser);
                            if (NetworkConstant.isActive) {
                                NetworkConstant.devId = response.optJSONObject("result").optString("devId");
                                ActivityLogConstant.saveDevId("devId", NetworkConstant.devId, thirdUser);
                                NetworkConstant.sessionCount = response.optJSONObject("result").optInt("sessionCount");
                                if (NetworkConstant.viewDicVer == null || NetworkConstant.viewDicVer == 0 || NetworkConstant.viewDicVer != response.optJSONObject("result").optInt("viewVer")) {
                                    if (response.optJSONObject("result").has("viewVer") && response.optJSONObject("result").has("viewDic")) {
                                        NetworkConstant.viewDicVer = response.optJSONObject("result").optInt("viewVer");
                                        NetworkConstant.viewDic = response.optJSONObject("result").optJSONObject("viewDic");
                                        ActivityLogConstant.saveViewDicVer("viewDicVer", NetworkConstant.viewDicVer, thirdUser);
                                        ActivityLogConstant.saveViewDic("viewDic", NetworkConstant.viewDic, thirdUser);
                                    }
                                }
                                if (NetworkConstant.screenVer == null || NetworkConstant.screenVer == 0 || NetworkConstant.screenVer != response.optJSONObject("result").optInt("screenVer")) {
                                    if (response.optJSONObject("result").has("screenVer") && response.optJSONObject("result").has("screenArr")) {
                                        NetworkConstant.screenSet = ActivityLogConstant.convertToSet(response.optJSONObject("result").optJSONArray("screenArr").toString());
                                        NetworkConstant.screenVer = response.optJSONObject("result").optInt("screenVer");
                                        ActivityLogConstant.saveScreenVer("screenVer", NetworkConstant.screenVer, thirdUser);
                                        ActivityLogConstant.saveScreenSet("screenSet", NetworkConstant.screenSet, thirdUser);
                                    }
                                }
                                HashMap<String, ArrayList<Long>> savedMap = ActivityLogConstant.getEventMap("eventMap", thirdUser); //-> use this later
                                getMySurvey(ActivityLogConstant.actAuthKey, NetworkConstant.devId, savedMap);
                            }
                        }
                    }
                    @Override
                    public void onDownloadFail(boolean fail, Throwable e) {
                        Log.e("setSDKUser", "failed" + e.toString());
                    }
                });
                SdkLifecycle.init(currApp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setUser(String userId) {
        NetworkConstant.userId = userId;
        NetworkConstant.isSetUserCalled = true;
    }

    public static void setLogUserAttributes(String key, String value) {
        if (key == null || value == null) {
            return;
        }
        NetworkConstant.isLogUserAttCalled = true;
        try {
            NetworkConstant.userAttri.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void setLogUserAttributes(String key, Integer value) {
        if (key == null || value == null) {
            return;
        }
        NetworkConstant.isLogUserAttCalled = true;
        try {
            NetworkConstant.userAttri.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void setLogUserAttributes(String key, Boolean value) {
        if (key == null || value == null) {
            return;
        }
        NetworkConstant.isLogUserAttCalled = true;
        try {
            NetworkConstant.userAttri.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void setLogEvent(String eventName, String type) {
        if (eventName == null || type == null || currApp == null) {
            return;
        }
        ArrayList<Long> tempArr = new ArrayList<>();
        Long lastUsed = SdkLifecycle.getDateAndTime();

        HashMap<String, ArrayList<Long>> savedMap = ActivityLogConstant.getEventMap("eventMap", currApp.getApplicationContext());
        if (savedMap != null) {
            Long count = savedMap.containsKey(eventName) ? savedMap.get(eventName).get(0) : 0L;


            tempArr.add(count + 1L);
            tempArr.add(lastUsed);

            savedMap.put(eventName, tempArr);

        } else {
            savedMap = new HashMap<>();
            tempArr.add(1L);
            tempArr.add(lastUsed);
            savedMap.put(eventName, tempArr);
        }
        ActivityLogConstant.saveEventMap("eventMap", savedMap, currApp.getApplicationContext());

        NetworkConstant.eventName = eventName;
        NetworkConstant.eventType = type;
        NetworkConstant.eventTS = (double) SdkLifecycle.getDateAndTime();
        if (ActivityLogConstant.actAuthKey != null && NetworkConstant.devId != null && NetworkConstant.eventType != null && NetworkConstant.eventName != null && NetworkConstant.eventTS != null) {
            try {
                nm.setLogEvent(ActivityLogConstant.actAuthKey, NetworkConstant.devId, NetworkConstant.eventTS, NetworkConstant.eventName, NetworkConstant.eventType, new NetworkConnect.CallbackInterface() {
                    @Override
                    public void onDownloadSuccess(boolean success, JSONObject response) {
                        Log.i("logEvent", response.toString());
                    }

                    @Override
                    public void onDownloadFail(boolean fail, Throwable e) {
                        Log.i("logEvent", "failed" + e.toString());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    protected static void getMySurvey(final String aKey, final String devId, HashMap<String, ArrayList<Long>> savedMap) {
        try {
            nm.getMySurverys(aKey, devId, savedMap, new NetworkConnect.CallbackInterface() {
                @Override
                public void onDownloadSuccess(boolean success, JSONObject response) {
                    Log.i("getMySurvey", response.toString());
                    if (response.optJSONObject("result").has("available")) {
                        JSONArray jsonArray = response.optJSONObject("result").optJSONArray("available");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Log.i("getJsonOb", jsonArray.optJSONObject(i).toString());
                            if (jsonArray.optJSONObject(i).has("promptView")) {
                                Log.i("tempPromptView", jsonArray.optJSONObject(i).optJSONObject("promptView").optString("android"));
                                Log.i("tempSurveyId", jsonArray.optJSONObject(i).optString("objectId"));
                                String tempPromptView = jsonArray.optJSONObject(i).optJSONObject("promptView").optString("android");
                                String tempSurveyId = jsonArray.optJSONObject(i).optString("objectId");
                                Boolean tempSkippable = jsonArray.optJSONObject(i).optBoolean("isSkippable");
                                ArrayList<HashMap<String, Boolean>> tempList;
                                if (NetworkConstant.hasPromptViewMap.containsKey(tempPromptView)) {
                                    HashMap<String, Boolean> tempMap = new HashMap<>();
                                    tempMap.put(tempSurveyId, tempSkippable);
                                    tempList = NetworkConstant.hasPromptViewMap.get(tempPromptView);
                                    tempList.add(tempMap);
                                } else {
                                    tempList = new ArrayList<>();
                                    HashMap<String, Boolean> tempMap = new HashMap<>();
                                    tempMap.put(tempSurveyId, tempSkippable);
                                    tempList.add(tempMap);
                                }
                                NetworkConstant.hasPromptViewMap.put(tempPromptView, tempList);
                            } else {
                                Log.i("promptView", "No promptView in the response");
                                NetworkConstant.noPromptView.add(jsonArray.optJSONObject(i));
                            }
                        }
                        if (jsonArray.length() != 0 && !jsonArray.optJSONObject(0).has("promptView")) {
                            String surId = jsonArray.optJSONObject(0).optString("objectId");
                            Boolean isSkippable = jsonArray.optJSONObject(0).optBoolean("isSkippable");
                            ActivityLogConstant.saveSurvId("survId", surId, thirdUser);
                            SurveyAlertManager.showDialog(thirdUser, aKey, surId, devId, isSkippable);
                        }
                    }
                }

                @Override
                public void onDownloadFail(boolean fail, Throwable e) {
                    Log.i("getMySurver", e.toString());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /*
        SurveyAlertManager.showDialog(thirdUser);
*/

}
